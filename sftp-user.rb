#This script only works on UBUNTU and DEBIAN
#Document root
def start
    backuping_ssh
end
def backuping_ssh
    if  system("sshd -t")
            puts "Backuping ssh config file on /etc/ssh/sshd_config.backup"
            system("cp -p /etc/ssh/sshd_config /etc/ssh/sshd_config.backup")
            read_ssh_config
    else
            puts""
            puts "You have errors on your ssh config file syntax, please make sure it is fixed before run this script again"
            puts "Exiting..."
    end
end
def read_ssh_config
    str = File.read("/etc/ssh/sshd_config")
    if str.match(/^Subsystem sftp internal-sftp/)
            puts "\"sftp internal-sftp\" Already configured on SSH Config file"
    else
            puts "Configuring \"sftp internal-sftp\" on SSH Config file..."
            str = str.gsub("Subsystem","#Subsystem")
            str += "\nSubsystem sftp internal-sftp\nMatch group ftpaccess\nChrootDirectory %h\n X11Forwarding no\nAllowTcpForwarding no\nForceCommand internal-sftp -u 0002"
            write_ssh_config(str)
    end
end
            def write_ssh_config(text)
                File.open("/etc/ssh/sshd_config", "r+").write(text)
                puts ""
                puts "Done."
                restart_ssh
            end
            def restart_ssh
               if  system("sshd -t")
                    system("service ssh restart")
                    puts "SSH Restarted"
                    create_group
               else
                       puts ""
                       puts "Check ERROR of above ^"
                       puts "Could not restart SSH - Rolling back"
                       rollback_ssh
               end   
            end
            def rollback_ssh
                    puts "Rolled back from backup in /etc/ssh/ path" if system("cp -a /etc/ssh/sshd_config.backup /etc/ssh/sshd_config")
                    abort
            end
            def create_group 
                if system("addgroup sftp")
                        puts ("Group sftp created successfully")
                end
                create_user
            end 
            def create_user
                    print "Enter username: "
                    $username=gets.chomp
                    if system("useradd -m #{$username} -g sftp -s /usr/sbin/nologin")
                        puts "User \"#{$username}\" created successfully"
                        until system("passwd #{$username}") 
                              puts "ERR - Try again"
                        end     
                        get_path_jail              
                    else        
                        puts "Please try with another user name"
                        create_user
                    end
            end             

           def get_path_jail
                    print "Enter path where user will be jailed: "
                    $path_jail= gets.chomp
                    if Dir.exist?("#{$path_jail}")
                        final_steps 
                    else        
                    #final_steps if exec("ls #{$path_jail}")
                    puts "Path doesnt exists try again"
                    get_path_jail
                    end
                    
            end
           def final_steps
                puts "Changing proper permissions"
                abort if !system("chown root /home/#{$username}") 
                abort if !system("mkdir -p /home/#{$username}/sftp/public_html/") 
                abort if !system("chown -R #{$username}:sftp /home/#{$username}/sftp/public_html") 
                abort if !system("mount --bind #{$path_jail} /home/#{$username}/sftp/public_html") 
                abort if !system("find #{$path_jail} -type d -exec chmod -R 775 {} \\; ") 
                abort if !system("find #{$path_jail} -type f -exec chmod -R 664 {} \\; ") 
                abort if !File.open("/etc/fstab","a+").write("#{$path_jail} /home/#{$username}/sftp/public_html none defaults,bind 0 0\n")            
                abort if !system("chown www-data:www-data /home/#{$username}/sftp/public_html") 
                puts ""
                print "username of web server [www-data]: "
                webserver=gets.chomp
                if webserver.empty?
                      system("usermod -a -G www-data #{$username}")
                else
                      system("usermod -a -G #{webserver} #{$username}")
                end
                system("usermod -a -G sftp www-data")  
                puts "Done" 
                puts "Make sure you have \"PasswordAuthenticate yes\" on you ssh config file , if not please add and restart ssh service. \n Make sure you /etc/fstab is correct before you restart you server"
                 
           end
           
        
            start